//
//  LMDeliveryDataEntrySpecs.swift
//  LMDelivery
//
//  Created by Ying Wai Shum on 22/12/2018.
//  Copyright © 2018年 Ying Wai Shum. All rights reserved.
//
import Quick
import Nimble
import SwiftyJSON
import ObjectMapper

@testable import LMDelivery

class LMDeliveryDataEntrySpecs: QuickSpec {
    override func spec() {
        var deliveryDataList: [LMDeliveryDataEntry] = []
        describe("LMDeliveryDataEntry") {
            context("LMDelivery Data Checking") {
                afterEach {
                    deliveryDataList = []
                }
                beforeEach {
                    if let path = Bundle(for: type(of: self)
                        ).path(forResource: "lmdelivery_dummy", ofType: "json") {
                        do {
                            if let jsonData = try? Data(contentsOf: URL(fileURLWithPath: path)){
                                let json = try JSON(data: jsonData)
                                if let array = json.array{
                                    // do something with array
                                    for item in array{
                                        if let itemDict = item.dictionaryObject,
                                            let deliveryData = LMDeliveryDataEntry(JSON: itemDict){
                                            deliveryDataList.append(deliveryData)
                                        }
                                    }
                                }
                            }
                        } catch {
                            fail("Problem parsing JSON")
                        }
                    }
                }
                it("Delivery Data Totals"){
                    print("deliveryDataList: \(deliveryDataList.count)")
                    expect(deliveryDataList.count).to(equal(2))
                }
                it("Delivery Data 1"){
                    if deliveryDataList.count == 2{
                        let deliveryData1 = deliveryDataList[0]
                        if let id = deliveryData1.id{
                            expect(id).to(equal(0))
                        }
                        if let desc = deliveryData1.description{
                            expect(desc).to(equal("Deliver documents to Andrio"))
                        }
                        if let imageUrl = deliveryData1.imageUrl{
                            expect(imageUrl).to(equal("https://www.what-dog.net/Images/faces2/scroll0015.jpg"))
                        }
                      
                    
                        if let location = deliveryData1.location{
                            if let lat = location.lat{
                                expect(lat).to(equal(22.336093))
                            }
                            if let lng = location.lng{
                                expect(lng).to(equal(114.155288))
                            }
                            if let address = location.address{
                                expect(address).to(equal("Cheung Sha Wan"))
                            }
                        }
                    }
                    else{
                        fail()
                    }
                }
                it("Delivery Data 2"){
                    if deliveryDataList.count == 2{
                        let deliveryData1 = deliveryDataList[1]
                        if let id = deliveryData1.id{
                            expect(id).to(equal(1))
                        }
                        if let desc = deliveryData1.description{
                            expect(desc).to(equal("Deliver parcel to Leviero"))
                        }
                        if let imageUrl = deliveryData1.imageUrl{
                            expect(imageUrl).to(equal("http://www.memoryandjustice.org/wp-content/uploads/2017/10/impossibly-cute-puppy-8.jpg"))
                        }
                        
                        if let location = deliveryData1.location{
                            if let lat = location.lat{
                                expect(lat).to(equal(22.319181))
                            }
                            if let lng = location.lng{
                                expect(lng).to(equal(114.170008))
                            }
                            if let address = location.address{
                                expect(address).to(equal("Mong Kok"))
                            }
                        }
                    }
                    else{
                        fail()
                    }
                }
              
            }
        }
    }
}
