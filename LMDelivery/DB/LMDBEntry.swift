//
//  LMDBEntry.swift
//  LMDelivery
//
//  Created by Ying Wai Shum on 22/12/2018.
//  Copyright © 2018年 Ying Wai Shum. All rights reserved.
//

import Foundation
import RealmSwift

class LMDeliveryRealmEntry: Object{
    @objc dynamic var id: Int = -1
    @objc dynamic var desc: String = ""
    @objc dynamic var imageUrl: String = ""
    @objc dynamic var lat: Double = -1
    @objc dynamic var lng: Double = -1
    @objc dynamic var address: String = ""
    override static func primaryKey() -> String? {
        return "id"
    }
}


