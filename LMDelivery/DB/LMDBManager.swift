//
//  LMDBManager.swift
//  LMDelivery
//
//  Created by Ying Wai Shum on 23/12/2018.
//  Copyright © 2018年 Ying Wai Shum. All rights reserved.
//

import Foundation
import RealmSwift

class LMDBManager {
    private var database:Realm
    static let shared = LMDBManager()
    
    private init() {
        database = try! Realm()
    }
    
    //MARK: General methods for data entries to DB
    static func storeDataToDB(dataEntries: [LMDeliveryDataEntry]){
        for data in dataEntries{
            let dbEntry = LMDeliveryRealmEntry()
            if let id = data.id{
                dbEntry.id = id
            }
            if let desc = data.description{
                dbEntry.desc = desc
            }
            if let imageUrl = data.imageUrl{
                dbEntry.imageUrl = imageUrl
            }
            if let loc = data.location{
                if let lat = loc.lat{
                    dbEntry.lat = lat
                }
                if let lng = loc.lng{
                    dbEntry.lng = lng
                }
                if let address = loc.address{
                    dbEntry.address = address
                }
            }
            LMDBManager.shared.addData(object: dbEntry)
        }
    }
    static func retrieveDataFromDB() -> [LMDeliveryDataEntry]{
        var dataEntries: [LMDeliveryDataEntry] = []
        let dbEntries = LMDBManager.shared.getDataFromDB()
        for dbEntry in dbEntries{
            var dataEntry = LMDeliveryDataEntry()
            var dataLocEntry = LMDeliveryLocDataEntry()
            dataEntry.id = dbEntry.id
            dataEntry.description = dbEntry.desc
            dataEntry.imageUrl = dbEntry.imageUrl
            dataLocEntry.lat = dbEntry.lat
            dataLocEntry.lng = dbEntry.lng
            dataLocEntry.address = dbEntry.address
            dataEntry.location = dataLocEntry
            dataEntries.append(dataEntry)
        }
        return dataEntries
    }
    
    
 
    //MARK: DB save and load methods
    func getDataFromDB() ->  Results<LMDeliveryRealmEntry> {
        let results: Results<LMDeliveryRealmEntry> =   database.objects(LMDeliveryRealmEntry.self)
        return results
    }
    func addData(object: LMDeliveryRealmEntry)   {
        try! database.write {
            database.add(object, update: true)
            print("Added new object")
        }
    }
    func deleteAllFromDatabase()  {
        try!   database.write {
            database.deleteAll()
        }
    }
    func deleteFromDb(object: LMDeliveryRealmEntry)   {
        try!   database.write {
            database.delete(object)
        }
    }
}
