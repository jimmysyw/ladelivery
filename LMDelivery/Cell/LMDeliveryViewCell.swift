//
//  LMDeliveryViewCell.swift
//  LMDelivery
//
//  Created by Ying Wai Shum on 30/1/2019.
//  Copyright © 2019年 Ying Wai Shum. All rights reserved.
//

import UIKit
import Kingfisher
import QuartzCore

class LMDeliveryViewCell: UITableViewCell {

    var deliveryData : LMDeliveryDataEntry? {
        didSet {
            if let imageUrlStr = deliveryData?.imageUrl{
                if let url = URL(string: imageUrlStr){
                    deliveryImageView.kf.setImage(with: url,
                        options: [
                        .cacheOriginalImage
                        ]
                    )
                }
            }
            if let description = deliveryData?.description{
                if let address = deliveryData?.location?.address{
                    deliveryDescLabel.text = "\(description) at \(address)"
                }
                else{
                    deliveryDescLabel.text = description
                }
            }
            
        }
    }
    
    
    private let deliveryDescLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textAlignment = .left
        lbl.numberOfLines = 0
        return lbl
    }()
    
    private let deliveryImageView : LMDeliveryRoundImageView = {
        let imgView = LMDeliveryRoundImageView()
        imgView.contentMode = .scaleAspectFill
        return imgView
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(deliveryImageView)
        addSubview(deliveryDescLabel)
        
        
        deliveryImageView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: nil, paddingTop: 5, paddingLeft: 5, paddingBottom: 5, paddingRight: 0, width: 50, height: 50, enableInsets: false)
        deliveryDescLabel.anchor(top: topAnchor, left: deliveryImageView.rightAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 10, width: 0, height: 0, enableInsets: false)
        
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
