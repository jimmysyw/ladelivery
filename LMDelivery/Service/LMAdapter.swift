//
//  LMAdapter.swift
//  LMDelivery
//
//  Created by Jimmy Shum on 18/12/2018.
//  Copyright © 2018 Ying Wai Shum. All rights reserved.
//

import Foundation
import Moya
import Alamofire

class NetworkAdaptors{
    class DefaultAlamofireManager: Alamofire.SessionManager {
        static let sharedManager: DefaultAlamofireManager = {
            let configuration = URLSessionConfiguration.default
            configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
            configuration.timeoutIntervalForRequest = 20
            configuration.timeoutIntervalForResource = 20
            configuration.requestCachePolicy = .useProtocolCachePolicy
            return DefaultAlamofireManager(configuration: configuration)
        }()
        
        static func manager(timeout: Int) -> DefaultAlamofireManager {
            let configuration = URLSessionConfiguration.default
            configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
            configuration.timeoutIntervalForRequest = TimeInterval(timeout)
            configuration.timeoutIntervalForResource = TimeInterval(timeout)
            configuration.requestCachePolicy = .useProtocolCachePolicy
            return DefaultAlamofireManager(configuration: configuration)
        }
    }
}


class LMAdapter{
    static func request(target: LMService,
                        success successCallback: @escaping (Response) -> Void,
                        error errorCallback: @escaping (Swift.Error) -> Void,
                        failure failureCallback: @escaping (MoyaError) -> Void) -> Cancellable {
        
        
        /// Configure manager, especially for request timeout
        let manager = NetworkAdaptors.DefaultAlamofireManager.manager(timeout: 60)
        
        let lmProvider = MoyaProvider<LMService>(manager: manager, plugins: [NetworkLoggerPlugin(verbose: true)])
        
        
        let token = lmProvider.request(target) { (result) in
            switch result {
            case .success(let response):
                do {                            //200 - 300
                    let _ = try response.filterSuccessfulStatusCodes()
                    successCallback(response)
                }
                catch let error {               //404, 500
                    do {
                        if let errorResponse = error as? Moya.MoyaError,
                            let r = errorResponse.response {
                            let d = r.data
                            let json = try JSONSerialization.jsonObject(with: d, options: []) as? [String: Any]
                            
                            print("\(r.debugDescription) : \(String(describing: json))")
                        }
                        else {
                            errorCallback(error)
                        }
                    }
                    catch let e {
                        errorCallback(e)
                    }
                    
                }
            case .failure(let error):       //Fail with error
                failureCallback(error)
            }
        }
        
        return token
    }
}
