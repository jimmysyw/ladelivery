//
//  LMService.swift
//  LMDelivery
//
//  Created by Jimmy Shum on 18/12/2018.
//  Copyright © 2018 Ying Wai Shum. All rights reserved.
//

import Foundation
import Moya
import Alamofire

enum LMService{
    case delivery(offset: Int, limit: Int)
}

extension LMService: TargetType{
    var baseURL: URL{
        return URL(string: "https://mock-api-mobile.dev.lalamove.com")!
    }
    
    var path: String{
        return "/deliveries"
    }
    
    var method: Moya.Method{
        return .get
    }
    
    var headers: [String : String]?{
        return nil
    }
    
    var sampleData: Data{
        return Data()
    }
    
    var task: Task{
        return .requestParameters(parameters: self.parameters, encoding:JSONEncoding.default)
    }
    
    var parameters: [String: Any]{
        switch self {
        case .delivery(let offset,let limit):
            return [
                "offset": offset,
                "limit": limit
            ]
        }
    }
    
  
    
    
}
