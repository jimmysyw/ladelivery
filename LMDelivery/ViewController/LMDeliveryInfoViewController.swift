//
//  LMDeliveryInfoViewController.swift
//  LMDelivery
//
//  Created by Ying Wai Shum on 30/1/2019.
//  Copyright © 2019年 Ying Wai Shum. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class LMDeliveryInfoViewController: UIViewController {
    var infoViewModel: LMDeliveryInfoViewModel
    
    init(infoViewModel: LMDeliveryInfoViewModel) {
        self.infoViewModel = infoViewModel
        super.init(nibName: nil, bundle: nil)
        self.view.backgroundColor = UIColor.white        
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        self.view = LMDeliveryInfoView(deliveryData: infoViewModel.deliveryData)
    }
    override func viewDidLoad() {
        title = "Delivery Detail"
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        edgesForExtendedLayout = []
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
