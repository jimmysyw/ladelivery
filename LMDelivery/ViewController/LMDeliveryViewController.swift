//
//  LMDeliveryViewController.swift
//  LMDelivery
//
//  Created by Ying Wai Shum on 30/1/2019.
//  Copyright © 2019年 Ying Wai Shum. All rights reserved.
//

import UIKit
import JGProgressHUD

class LMDeliveryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var deliveryTableView: UITableView?
    var deliveryViewModel: LMDeliveryViewModel?
    var progressView: JGProgressHUD?
    var refreshControl: UIRefreshControl?
    var paginationControl: UIActivityIndicatorView?
    var deliveryInfoVC: LMDeliveryInfoViewController?
    
    init(deliveryViewModel: LMDeliveryViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.deliveryViewModel = deliveryViewModel
        self.view.backgroundColor = UIColor.white
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        setupViewModel()
        setupView()
        setupNotificationObserver()
        
        startLoadingView()
    }
    func setupViewModel(){
        deliveryViewModel?.viewModelDidUpdate = {
            self.hideLoadingView()
            self.deliveryTableView?.reloadData()
            self.deliveryTableView?.refreshControl?.endRefreshing()
            self.paginationControl?.stopAnimating()
        }
        deliveryViewModel?.viewModelDataDidError = {
            self.promptAlert(title: "Error", desc: "Error occured when retrieving delivery data")
            self.hideLoadingView()
            self.deliveryTableView?.refreshControl?.endRefreshing()
            self.paginationControl?.stopAnimating()
        }
        deliveryViewModel?.viewModelDataDidFailure = {
            self.promptAlert(title: "Failure", desc: "Fail to retrieve delivery data")
            self.hideLoadingView()
            self.deliveryTableView?.refreshControl?.endRefreshing()
            self.paginationControl?.stopAnimating()
        }
    }
    
    func setupView(){
        self.title = "LMDelivery"
        navigationController?.navigationBar.backgroundColor = UIColor.gray
        setupTableView()
        setupRefreshControl()
        setupPaginationControl()
        setupLoadingView()
    }
    func setupTableView(){
        deliveryTableView = UITableView()
        deliveryTableView?.frame = self.view.frame
        deliveryTableView?.register(LMDeliveryViewCell.self, forCellReuseIdentifier: "LMDeliveryViewCell")
        deliveryTableView?.delegate = self
        deliveryTableView?.dataSource = self

        if let tableView = deliveryTableView{
            self.view.addSubview(tableView)
        }
    }
    
    func setupLoadingView(){
        progressView = JGProgressHUD(style: .dark)
        progressView?.textLabel.text = "Loading"
        
    }
    
    func startLoadingView(){
        if let navView = self.navigationController?.view{
            progressView?.show(in: navView)
        }
    }
    func hideLoadingView(){
        progressView?.dismiss()
    }
    
    
    
    func setupRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(LMDeliveryViewController.refreshDeliveryView), for: .valueChanged)
        deliveryTableView?.refreshControl = refreshControl
    }
    func setupPaginationControl(){
        paginationControl = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        paginationControl?.color = UIColor.darkGray
        paginationControl?.hidesWhenStopped = true
        deliveryTableView?.tableFooterView = paginationControl
    }
    
    
    func setupNotificationObserver() {
        NotificationCenter.default.addObserver(forName: DataStoreNotification.dataRetrieveSuccess.name,
                                               object: nil,
                                               queue: OperationQueue.main,
                                               using: { [weak self] (_) in
                                                self?.hideLoadingView()
                                                self?.deliveryViewModel?.updateDataEntries()
        })
        NotificationCenter.default.addObserver(forName: DataStoreNotification.dataRetrieveError.name,
                                               object: nil,
                                               queue: OperationQueue.main,
                                               using: { [weak self] (_) in
                                                self?.hideLoadingView()
                                                
                                                
        })
        NotificationCenter.default.addObserver(forName: DataStoreNotification.dataRetrieveFail.name,
                                               object: nil,
                                               queue: OperationQueue.main,
                                               using: { [weak self] (_) in
                                                self?.hideLoadingView()
                                                
        })
    }
    @objc func refreshDeliveryView(refreshControl: UIRefreshControl){
        print("refreshDeliveryView: \(refreshControl)")
        //        deliveryViewModel?.updateDataEntries()
        deliveryViewModel?.fetchDataEntries(isDataReset: true)
        
    }
    //MARKS: Table View Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let deliveryData = deliveryViewModel?.deliveryDataEntries[indexPath.row]{
            self.navigationController?.pushViewController(LMDeliveryInfoViewController(infoViewModel: .init(deliveryData: deliveryData)), animated: true)
        }

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = deliveryViewModel?.deliveryDataEntries.count{
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LMDeliveryViewCell", for: indexPath) as? LMDeliveryViewCell else { return UITableViewCell() }
        if let deliveryData = deliveryViewModel?.deliveryDataEntries[indexPath.row]{
            cell.deliveryData = deliveryData
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let count = deliveryViewModel?.deliveryDataEntries.count{
            let lastElement = count - 1
            if indexPath.row == lastElement{
                deliveryViewModel?.fetchDataEntries(isDataReset: false)
                paginationControl?.startAnimating()
            }
        }
       
    }
}
