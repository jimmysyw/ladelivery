//
//  LMDeliveryInfoViewModel.swift
//  LMDelivery
//
//  Created by Ying Wai Shum on 22/12/2018.
//  Copyright © 2018年 Ying Wai Shum. All rights reserved.
//

import UIKit

class LMDeliveryInfoViewModel{
    var deliveryData: LMDeliveryDataEntry
    var viewModelDidUpdate: (() -> Void)?
    
    init(deliveryData: LMDeliveryDataEntry){
        self.deliveryData = deliveryData
        setupViewModel()
    }
    func setupViewModel(){
        
    }
}
