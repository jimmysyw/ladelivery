//
//  LMDeliveryViewModel.swift
//  LMDelivery
//
//  Created by Ying Wai Shum on 22/12/2018.
//  Copyright © 2018年 Ying Wai Shum. All rights reserved.
//

import Foundation

class LMDeliveryPagedDataEntry{
    var deliveryDataEntries: [LMDeliveryDataEntry] = []
    init(deliveryDataEntries: [LMDeliveryDataEntry]){
        self.deliveryDataEntries = deliveryDataEntries
    }
}

class LMDeliveryViewModel{
    var deliveryDataEntries: [LMDeliveryDataEntry] = []
    var deliveryDataPagedDataEntries: [LMDeliveryPagedDataEntry] = []
    var viewModelDidUpdate: (() -> Void)?
    var viewModelDataDidError: (() -> Void)?
    var viewModelDataDidFailure: (() -> Void)?
    
    
    init(){
        setupViewModel()
    }
    func setupViewModel(){
        setupNotificationObserver()
        updateDataEntries()
    }
    func fetchDataEntries(isDataReset: Bool){
        LMDataStore.shared.requestDeliveryDataList(isDataReset: isDataReset)
    }
    
    func updateDataEntries(){
        deliveryDataEntries = LMDataStore.shared.deliveryDataList
        print("deliveryDataEntries: \(deliveryDataEntries)")
        viewModelDidUpdate?()
    }
    
    func setupNotificationObserver() {
        NotificationCenter.default.addObserver(forName: DataStoreNotification.dataRetrieveSuccess.name,
                                               object: nil,
                                               queue: OperationQueue.main,
                                               using: { [weak self] (_) in
                                                self?.updateDataEntries()
        })
        NotificationCenter.default.addObserver(forName: DataStoreNotification.dataRetrieveError.name,
                                               object: nil,
                                               queue: OperationQueue.main,
                                               using: { [weak self] (_) in
                                                self?.viewModelDataDidError?()
                                                
        })
        NotificationCenter.default.addObserver(forName: DataStoreNotification.dataRetrieveFail.name,
                                               object: nil,
                                               queue: OperationQueue.main,
                                               using: { [weak self] (_) in
                                                self?.viewModelDataDidFailure?()
                                        
        })
    }
}
