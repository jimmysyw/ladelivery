//
//  LMDeliveryDataEntry.swift
//  LMDelivery
//
//  Created by Ying Wai Shum on 22/12/2018.
//  Copyright © 2018年 Ying Wai Shum. All rights reserved.
//

import Foundation
import ObjectMapper

struct LMDeliveryLocDataEntry: Mappable{
    var lat: Double?
    var lng: Double?
    var address: String?
    
    init(){
        
    }
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        lat         <- map["lat"]
        lng         <- map["lng"]
        address     <- map["address"]
    }
}

struct LMDeliveryDataEntry: Mappable{
    var id: Int?
    var description: String?
    var imageUrl: String?
    var location: LMDeliveryLocDataEntry?
    
    init(){
        
    }
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id          <- map["id"]
        description <- map["description"]
        imageUrl    <- map["imageUrl"]
        location    <- map["location"]

    }
}
