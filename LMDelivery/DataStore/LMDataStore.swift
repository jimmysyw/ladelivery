//
//  LMDataStore.swift
//  LMDelivery
//
//  Created by Jimmy Shum on 18/12/2018.
//  Copyright © 2018 Ying Wai Shum. All rights reserved.
//

import Foundation
import SwiftyJSON

enum DataStoreNotification: String, NotificationName {
    case dataRetrieveSuccess,dataRetrieveError,dataRetrieveFail
}

class LMDataStore{
    static let shared = LMDataStore()
    var deliveryDataList: [LMDeliveryDataEntry] = []
    func prepareData(){
        requestDeliveryDataList()
    }
    
    func requestDeliveryDataList(isDataReset: Bool = false){
        if isDataReset{
            deliveryDataList = []
        }
        _ = LMAdapter.request(
            target: .delivery(offset: 0, limit: 20),
            success: {response in
                let json = JSON(response.data)
                if let array = json.array{
                    // do something with array
                    for item in array{
                        if let itemDict = item.dictionaryObject,
                           let deliveryData = LMDeliveryDataEntry(JSON: itemDict){
                            self.deliveryDataList.append(deliveryData)
                        }
                    }
                }
//                print("deliveryDataList: \(self.deliveryDataList)")
                NotificationCenter.default.post(name: DataStoreNotification.dataRetrieveSuccess.name, object: nil)
        },
            error: { response in
                NotificationCenter.default.post(name: DataStoreNotification.dataRetrieveError.name, object: nil)
        },
            failure: { response in
                NotificationCenter.default.post(name: DataStoreNotification.dataRetrieveFail.name, object: nil)
        })
    }
    
    
}
