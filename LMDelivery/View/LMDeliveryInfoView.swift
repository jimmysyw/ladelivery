//
//  LMDeliveryInfoView.swift
//  LMDelivery
//
//  Created by Ying Wai Shum on 2/2/2019.
//  Copyright © 2019年 Ying Wai Shum. All rights reserved.
//

import UIKit
import Kingfisher
import MapKit


class LMDeliveryInfoView: UIView {
    var deliveryData : LMDeliveryDataEntry
    
    private let deliveryInfoMapView : MKMapView = {
        let mapView = MKMapView()
        return mapView
    }()
    
    let deliveryInfoDetailView: LMDeliveryInfoDetailView
    
    
    init(deliveryData: LMDeliveryDataEntry){
        deliveryInfoDetailView = LMDeliveryInfoDetailView(deliveryData: deliveryData)
        self.deliveryData = deliveryData
        super.init(frame: CGRect.zero)
        addSubview(deliveryInfoDetailView)
        addSubview(deliveryInfoMapView)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
 
    func setupView(){
        deliveryInfoMapView.anchor(top: topAnchor, left: leftAnchor, bottom: deliveryInfoDetailView.topAnchor, right: rightAnchor, paddingTop: 5, paddingLeft: 5, paddingBottom: 5, paddingRight: 5, width: 0, height: 0, enableInsets: false)
        deliveryInfoDetailView.anchor(top: deliveryInfoMapView.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 50, paddingRight: 10, width: 100, height: 60, enableInsets: false)
        
        
        if let lat = deliveryData.location?.lat,
            let lng = deliveryData.location?.lng{
            print("lat: \(lat), lng: \(lng)")
            let coord = CLLocationCoordinate2DMake(lat, lng)
            deliveryInfoMapView.region = MKCoordinateRegionMakeWithDistance(coord, 200, 200)
            let annotation = MKPointAnnotation()
            annotation.coordinate = coord
            deliveryInfoMapView.addAnnotation(annotation)
        }
    }
}


class LMDeliveryInfoDetailView: UIView{
    var deliveryData : LMDeliveryDataEntry
    
    private let deliveryInfoDescLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textAlignment = .left
        lbl.numberOfLines = 0
        return lbl
    }()
    
    private let deliveryInfoImageView : LMDeliveryRoundImageView = {
        let imgView = LMDeliveryRoundImageView()
        imgView.contentMode = .scaleAspectFill
        return imgView
    }()
    
    
    init(deliveryData: LMDeliveryDataEntry){
        self.deliveryData = deliveryData
        super.init(frame: CGRect.zero)
        addSubview(deliveryInfoImageView)
        addSubview(deliveryInfoDescLabel)
        setupView()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupView(){
        deliveryInfoImageView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: deliveryInfoDescLabel.leftAnchor, paddingTop: 5, paddingLeft: 5, paddingBottom: 5, paddingRight: 10, width: 50, height: 50, enableInsets: false)
        deliveryInfoDescLabel.anchor(top: topAnchor, left: deliveryInfoImageView.rightAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 10, width: 0, height: 30, enableInsets: false)

        
        if let imageUrlStr = deliveryData.imageUrl{
            if let url = URL(string: imageUrlStr){
                deliveryInfoImageView.kf.setImage(with: url,
                                                  options: [
                                                    .cacheOriginalImage
                    ]
                )
            }
        }
        
        if let description = deliveryData.description{
            if let address = deliveryData.location?.address{
                deliveryInfoDescLabel.text = "\(description) at \(address)"
            }
            else{
                deliveryInfoDescLabel.text = description
            }
        }
    }
}
