//
//  LMDeliveryRoundImageView.swift
//  LMDelivery
//
//  Created by Ying Wai Shum on 18/2/2019.
//  Copyright © 2019年 Ying Wai Shum. All rights reserved.
//

import UIKit

class LMDeliveryRoundImageView: UIImageView {
    override var bounds: CGRect {
        get {
            return super.bounds
        }
        set {
            super.bounds = newValue
            setNeedsLayout()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.width / 2.0
        clipsToBounds = true
    }
}
